from datetime import datetime
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework import viewsets


from .models import House, Apartment, Counter, Tariff
from .serializers import HouseSerializer, ApartmentSerializer, WaterMeterSerializer, TariffSerializer
from .calculations import calculate_utility_bills


class HouseViewSet(viewsets.ModelViewSet):
    queryset = House.objects.all()
    serializer_class = HouseSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        data = []
        for item in serializer.data:
            item['apartment_quantity'] = len(Apartment.objects.filter(house=item['id']))
            data.append(item)
        return Response(data)


class ApartmentViewSet(viewsets.ModelViewSet):
    queryset = Apartment.objects.all()
    serializer_class = ApartmentSerializer

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)
        data = []
        for item in serializer.data:
            item['address'] = House.objects.filter(id=item['house'])[0].address
            data.append(item)
        return Response(data)


class WaterMeterViewSet(viewsets.ModelViewSet):
    queryset = Counter.objects.all()
    serializer_class = WaterMeterSerializer


class TariffViewSet(viewsets.ModelViewSet):
    queryset = Tariff.objects.all()
    serializer_class = TariffSerializer


@api_view(['POST'])
def start_calculation(request):
    date = request.data.get('date')
    task = calculate_utility_bills.delay(date)
    return Response({'task-status': "start" + task.id })


@api_view(['GET'])
def get_progress(request, task_id):
    from celery.result import AsyncResult
    result = AsyncResult(task_id)
    return Response({'status': result.status, 'result': result.result})