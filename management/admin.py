from django.contrib import admin
from .models import House, Apartment, Counter, Tariff, CounterData, ApartmentBills

# Register your models here.
admin.site.register(House)
admin.site.register(Apartment)
admin.site.register(Counter)
admin.site.register(Tariff)
admin.site.register(CounterData)
admin.site.register(ApartmentBills)