from .models import Apartment, Counter, CounterData, ApartmentBills
from datetime import datetime
from dateutil.relativedelta import relativedelta
from celery import shared_task


@shared_task
def calculate_utility_bills(date):
    date = datetime.strptime(date, "%m.%Y")
    print(date)
    apartments = Apartment.objects.all()

    for apartment in apartments:
        water_counters = Counter.objects.filter(apartment=apartment.id, type='Water Supply')

        water_cost = 0
        if water_counters:
            for counter in water_counters:
                previous_month_date = datetime.now() + relativedelta(months=-1)

                previous_water_consumption = CounterData.objects.filter(counter=counter.id,
                                                                        date__year=previous_month_date.year,
                                                                        date__month=previous_month_date.month)
                month_water_consumption = CounterData.objects.filter(counter=counter.id,
                                                                     date__year=date.year,
                                                                     date__month=date.month)
                current_readings = month_water_consumption[0].count - previous_water_consumption[0].count

                water_cost = water_cost + counter.tariff.tariff * current_readings

        maintenance_count = apartment.tariff.tariff * apartment.area

        apartment_bill = ApartmentBills.objects.create(apartment=apartment,
                                                       water=water_cost,
                                                       maintenance=maintenance_count,
                                                       date=date)
        apartment_bill.save()
