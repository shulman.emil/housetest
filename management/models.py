from django.db import models


# Create your models here.
class House(models.Model):
    address = models.CharField(max_length=30, help_text='Адрес дома')


class Apartment(models.Model):
    number = models.IntegerField(help_text='Номер квартиры')
    area = models.DecimalField(max_digits=12, decimal_places=2, help_text='Площадь квартиры')
    house = models.ForeignKey(House, on_delete=models.CASCADE, help_text='Дом')
    tariff = models.ForeignKey('Tariff', default=1, on_delete=models.SET_DEFAULT, help_text='Тариф')


class Counter(models.Model):
    type = models.CharField(max_length=30, help_text='Тип счетчика')
    apartment = models.ForeignKey(Apartment, on_delete=models.CASCADE, help_text='Квартира')
    tariff = models.ForeignKey('Tariff', default=1, on_delete=models.SET_DEFAULT, help_text='Тариф')


class Tariff(models.Model):
    tariff = models.DecimalField(max_digits=7, decimal_places=2, help_text='Тариф')


class CounterData(models.Model):
    date = models.DateField(help_text='Дата показания')
    counter = models.ForeignKey('Counter', on_delete=models.CASCADE, help_text='Счетчик')
    count = models.DecimalField(max_digits=20, decimal_places=2, default=0, help_text='Показания')


class ApartmentBills(models.Model):
    apartment = models.ForeignKey('Apartment', on_delete=models.CASCADE, help_text='Квартира')
    water = models.DecimalField(max_digits=20, decimal_places=2, help_text='Водоснабжение')
    maintenance = models.DecimalField(max_digits=20, decimal_places=2, help_text='Содержание общего имущества')
    date = models.DateField(help_text='Дата счета')


